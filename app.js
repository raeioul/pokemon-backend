var express = require("express");
var app = express();
const axios = require("axios");
var fs = require("fs");
request = require("request");
const cors = require("cors");

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200
};

app.use(cors());

app.set("port", process.env.PORT || 1234);
app.get("/", function(req, res) {
  res.type("text/plain");
  res.send("Alive");
});

app.use(express.static("public"));

app.get("/pokemons", function(req, res) {
  res.type("text/html");
  var uri = `https://pokeapi.co/api/v2/pokemon/${req.query.name}`;
  filename = `${req.query.name}_front_default.png`;

  var download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
      console.log("content-type:", res.headers["content-type"]);
      console.log("content-length:", res.headers["content-length"]);
      request(uri)
        .pipe(fs.createWriteStream(`public/images/${filename}`))
        .on("close", callback);
    });
  };
  const path = `public/images/${filename}`;

  try {
    if (fs.existsSync(path)) {
      res.send(`<h1>${req.query.name}</h1><img src='images/${filename}'/>`);
    } else {
      axios.get(uri).then(function(response) {
        download(
          `${response.data.sprites["front_default"]}`,
          filename,
          function() {
            console.log("done");
          }
        );
        //console.log(response);
        res.send(
          `<h1>${response.data.forms[0].name}</h1><img src='${
            response.data.sprites["front_default"]
          }'/>`
        );
        res.send(response);
      });
    }
  } catch (err) {
    console.log(err);
  }
});

app.listen(app.get("port"), function() {
  console.log(
    "Express started on http://localhost:" +
      app.get("port") +
      "; press Ctrl-C to terminate."
  );
});
